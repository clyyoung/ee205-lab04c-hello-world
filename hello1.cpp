///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World 
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello1.cpp
/// @version 1.0
/// 
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 04c - Hello World - EE 205 - Spr 2021
/// @date   09 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

//don't need to qualify cout and endl since namespace is already identified
using namespace std;

int main(){
   cout << "Hello World!" << endl;

   return 0;
}
