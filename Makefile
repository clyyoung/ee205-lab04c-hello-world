###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04c - Hello World
#
# @file    Makefile
# @version 1.0
#
# @author Christianne Young <clyyoung@hawaii.edu>
# @brief  Lab 04c - Hello World - EE 205 - Spr 2021
# @date   09 Feb 2021
###############################################################################

# with make command, both programs compiled
TARGETS=hello1 hello2

all: $(TARGETS)

# target and dependencies for program 1
hello1: hello1.cpp
	g++ -o hello1 hello1.cpp

# target and dependencies for program 2
hello2: hello2.cpp
	g++ -o hello2 hello2.cpp

clean:
	rm -f $(TARGETS) *.o

